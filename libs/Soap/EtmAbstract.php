<?php

namespace Libs\Soap;

use Libs\Soap\MySoapClient;

abstract class EtmAbstract
{
    private $soap_client;
    private $settings_user;
    private $settings_password;
    private $settings_hashkey;

    public function __construct($settings)
    {

        $this->settings_user=$settings['certificates']['user'];
        $this->settings_password=$settings['certificates']['password'];
        $this->settings_hashkey=$settings['certificates']['hashkey'];


        $this->soap_client = new MySoapClient($settings['certificates']['wsdl'], array(
            'location' => $settings['certificates']['location'],
            'trace' => 1,
            'exceptions' => 1,
            'verifypeer' => false,
            'verifyhost' => false,
            'soap_version' => SOAP_1_2,
            'stream_context' => stream_context_create( array( 'ssl' => array(   'verify_peer' => false, 'verify_peer_name' => false, 'allow_self_signed' => true,) ) ),
            'certificate' => $settings['certificates']['certificate'],
            'certificate_key' => $settings['certificates']['certificate_key'],
            )
        );


/*
        $this->soap_client = new MySoapClient('https://soap.etm-system.ru/etm-webservice-0.3.1/server.php?WSDL', array(
            'location' => 'https://soap.etm-system.ru/etm-webservice-0.3.1/server.php',
            'trace' => 1,
            'exceptions' => 1,
            'verifypeer' => false,
            'verifyhost' => false,
            'soap_version' => SOAP_1_2,
            'stream_context' => stream_context_create( array( 'ssl' => array(   'verify_peer' => false, 'verify_peer_name' => false, 'allow_self_signed' => true,) ) ),
            )
        );
*/

    }
    
    public function getSecurityData()
    {


        $security = new \stdClass();
        
        $security->Username = new \SoapVar($this->settings_user, XSD_STRING);
        $security->Password = new \SoapVar($this->settings_password, XSD_STRING);
        $security->HashKey  = new \SoapVar($this->settings_hashkey, XSD_STRING);
        
        $security_soap = new \SoapVar($security, SOAP_ENC_OBJECT, 'SecurityType', null, 'Security');
        
        return $security_soap;
    }
    
    public function getSoapClient()
    {

        return isset($this->soap_client)
            ? $this->soap_client
            : new MySoapClient();
    }

    public function call()
    {
        try
        {
            $method_name = $this->getMethodName();

            $request = $this->getRequest();

            $response = $this->getSoapClient()->$method_name($request);
            
            if (isset($response->Errors))
            {
                return [
                    'error'    => true,
                    'code'     => isset($response->Errors->Code) ? $response->Errors->Code : 0,
                    'response' => isset($response->Errors->Message) ? $response->Errors->Message : ''];
            }
            
            return ['error' => false, 'response' => $response];

        } catch (\SoapFault $fault) {
            return ['error' => true, 'response' => $fault->getMessage()];
        }

    }
    
    abstract public function getRequest();
    abstract protected function getMethodName();
}
