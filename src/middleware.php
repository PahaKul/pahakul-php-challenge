<?php
// Application middleware

use Libs\Ping;

$etm_ping = function ($request, $response, $next) {
    $ping = new Ping($this->get('settings'));

    $ping_response = $ping->call();
    if (isset($ping_response['error']) && $ping_response['error'] == true) {
        return $this->renderer->render($response, 'etm_ping_error.phtml', compact('args', 'ping_response'));
    }
    return $next($request, $response);
};
