<?php
return [
    'settings' => [
        'displayErrorDetails' => true, // set to false in production
        'addContentLengthHeader' => false, // Allow the web server to send the content-length header


        'certificates' => [
            'user' => 'test1',
            'password' => '7mkn6XC9L54p',
            'hashkey' => 'b2cbf0f711',
            'wsdl' => 'https://soap.etm-system.ru/etm-webservice-0.3.1/server.php?WSDL',
            'location' => 'https://soap.etm-system.ru/etm-webservice-0.3.1/server.php',
            'certificate' => __DIR__ . '/../certificates/soap.etm-system.ru.crt',
            'certificate_key' => __DIR__ . '/../certificates/soap.etm-system.ru.key',
            'debug' => false,
        ],

        // Renderer settings
        'renderer' => [
            'template_path' => __DIR__ . '/../templates/',
        ],

        // Monolog settings
        'logger' => [
            'name' => 'slim-app',
            'path' => __DIR__ . '/../logs/app.log',
            'level' => \Monolog\Logger::DEBUG,
        ],
    ],
];
