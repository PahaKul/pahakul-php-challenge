<?php

namespace Tests\Functional;

class HomepageTest extends BaseTestCase
{
    /**
     * Test that the index route returns a rendered response containing the text 'SlimFramework' but not a greeting
     */
    public function testGetHomepage()
    {
        $response = $this->runApp('GET', '/');

        $this->assertEquals(200, $response->getStatusCode());
        $this->assertContains('Khabarovsk', (string)$response->getBody());
        $this->assertNotContains('Hello', (string)$response->getBody());
    }

    /**
     * Test that the index route with optional name argument returns a rendered greeting
     */

    public function testGetHomepageWithOutName()
    {
        $response = $this->runApp('GET', '/name');

        $this->assertNotEquals(200, $response->getStatusCode());
        $this->assertContains('Page Not Found', (string)$response->getBody());
    }

    /**
     * Test that the index route won't accept a post request
     */

    public function testPostHomepageNotAllowed()
    {
        $response = $this->runApp('POST', '/ajax/results/{request_id}' );

        $this->assertEquals(201, $response->getStatusCode());
        $this->assertContains('Sorry, no results received.', (string)$response->getBody());
    }

}